import pandas as pd

def firmas(matricula):
    col_names = ["Matricula", "Datetime", "IP", "OtherData"]
    asistencia = pd.read_csv('asistencia.csv', delimiter='\t', names = col_names)
    practicas = asistencia.loc[asistencia['Matricula'] == matricula]
    # filtrado = practicas.drop_duplicates(subset='Date')
    # print(f'{matricula},{filtrado.shape[0]}')
    return practicas[["Datetime", "IP"]].values.tolist()

if __name__ == "__main__":
    print (firmas('19016'))
