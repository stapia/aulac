from flask_restful import Resource
from flask import Flask, request
from datetime import datetime
import json

class Asistencia(Resource):

    def put(self, matricula):
        client_ip = request.environ.get('HTTP_X_REAL_IP', request.remote_addr)
        with open('asistencia.csv', 'a+') as f: 
            # text = f'{matricula}\t{datetime.now()}\t{client_ip}\t{json.dumps(request.form)}\n'
            text = '{matricula}\t{the_date}\t{client_ip}\t{the_request}\n'
            formatted_text = text.format(matricula=matricula, the_date=datetime.now(),
                client_ip=client_ip, the_request=json.dumps(request.form))
            f.write(formatted_text)
            return 'OK'
        return 'Error'
