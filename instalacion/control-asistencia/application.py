from flask import Flask, request
from flask_restful import abort, Api, Resource

from asistencia import Asistencia
from alumnos import Alumnos
from profesor import Profesor

app = Flask(__name__)
api = Api(app)

class HelloWorld(Resource):
    def get(self):
        return "Nada"

api.add_resource(HelloWorld, '/')

api.add_resource(Asistencia, '/asistencia/<matricula>')
api.add_resource(Alumnos, '/alumnos/<matricula>')
api.add_resource(Profesor, '/profesor/<desde>')

if __name__ == '__main__':
    app.run(debug=True)

application = app
