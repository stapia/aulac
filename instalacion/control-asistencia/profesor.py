from flask import Flask, request
from flask_restful import Resource
from datetime import datetime
import firmas_turno
from grupo_alumno import listado, alumno

class Profesor(Resource):

    def get(self, desde: str):
        if desde == 'ultimo':
            dt = firmas_turno.ultima_hora(datetime.now())
        else:
            dt = datetime.strptime(desde, "%Y-%m-%d-%H-%M")
            dt = firmas_turno.ultima_hora(dt)

        presentes = firmas_turno.firmas_ultimo_turno(dt)
        listado_alumnos = listado()

        for item in presentes:
            try:
                numero = int(item[0])
                datos = alumno(listado_alumnos, numero)
                if datos[1] == "Undefined":
                    item.append("No está en listas")
                else:
                    item.append(datos[1])
                    item.append(datos[2])
            except:
                item.append('El número de matricula no es un número')

        return {
            'desde': str(dt),
            'alumnos': presentes
        }
