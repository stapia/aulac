import pandas as pd
from datetime import datetime

def ultima_hora(ahora = datetime.now()):
    if ahora.minute >= 30:
        return datetime(ahora.year, ahora.month, ahora.day, ahora.hour, 30)
    else:
        return datetime(ahora.year, ahora.month, ahora.day, ahora.hour - 1, 30)

def firmas_ultimo_turno(desde: datetime):
    col_names = ["Matricula", "Datetime", "IP", "OtherData"]
    asistencia = pd.read_csv('asistencia.csv', delimiter='\t', names = col_names, parse_dates=['Datetime'])
    practicas = asistencia.loc[asistencia['Datetime'] >= desde]
    practicas['Datetime'] = practicas['Datetime'].apply( lambda dt: str(dt))
    return practicas[["Matricula", "Datetime", "IP"]].values.tolist()

if __name__ == "__main__":
    datesList = [
        datetime(2019,11,8,16,29),
        datetime(2019,11,8,16,30),
        datetime(2019,11,8,16,40),
        datetime(2019,11,8,12,20)
    ]

    for fecha in datesList:
        print(f"---- Desde {fecha} sin modificar: ----")
        print (firmas_ultimo_turno(fecha)) 

        ultima = ultima_hora(fecha)
        print(f"---- Desde {ultima} (modificada): ----")
        print (firmas_ultimo_turno(ultima)) 