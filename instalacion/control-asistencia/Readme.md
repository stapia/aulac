
# Instalación de Python3, Flask y demás #

```bash
sudo apt install python3-pip
```

Entrar en el root:

```bash
sudo su -
```

Y en root:

```bash
pip3 install Flask Flask-RESTful
```

Bajarse el código fuente de application.py y probar que va:

```bash
python3 application.py
```

Mirar en http://127.0.0.1:5000/ debe mostrar "nada"

Luego curl:

```bash
curl -X PUT -d 'data=hola' http://localhost:5000/asistencia/100
```

Y luego ya ha que configurar al apache2, una cosa que hay hacer 
es habilitar el modulo swgi:

```bash
sudo apt install libapache-mod-wsgi-py3
```

Además, la instalación habilita el modulo. Luego añadir el sitio:

```bash
sudo cp 008-py-app.conf /etc/apache2/sites-available/
```

y añadirlo:

```bash
sudo a2ensite 008-py-app.conf 
```

Y poner el puerto, hay que editar /etc/apache2/ports y añadir Listen 28080:

```bash
sudo nano /etc/apache2/ports
```

Rearrancar apache2: 

```bash
sudo systemctl restart apache2
```

Mirar en http://127.0.0.1:5000/ debe mostrar "nada".

Ahora hay que dar permisos de escritura para "otros" en la carpeta:

```bash
cd ..
chmod -R o+rw control-asistencia/
```

Y volver a probar:

```bash
curl -X PUT -d 'data=hola' http://138.100.77.232:28080/api/asistencia/100
```
