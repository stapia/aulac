
import pandas as pd

asistencia = pd.read_csv('asistencia.csv')
# print(asistencia.loc[asistencia['Matricula'] == '19745'])

for csv_file in ['55000630-listado_matriculados.csv', '55001007-listado_matriculados.csv']:
    print(f"Listado: {csv_file}")

    df = pd.read_csv(csv_file, encoding='windows-1252')
    # print( df.loc[df['Exp_centro'] >= 19000]['Exp_centro'] )

    for mat in df.loc[df['Exp_centro'] >= 19000]['Exp_centro']:
        practicas = asistencia.loc[asistencia['Matricula'] == str(mat)]
        filtrado = practicas.drop_duplicates(subset='Date')
        print(f'{mat},{filtrado.shape[0]}')