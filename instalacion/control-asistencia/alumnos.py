from flask import Flask, request
from flask_restful import Resource
from grupo_alumno import listado, alumno
from firmas import firmas

class Alumnos(Resource):

    def get(self, matricula):
        listado_alumnos = listado()
        numero = int(matricula)
        datos = alumno(listado_alumnos, numero)
        practicas = firmas(matricula)
        return { 'alumno': datos, 'asistencias': practicas }
