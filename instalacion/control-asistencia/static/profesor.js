
var apiURL = "/profesor/ultimo";

var app = new Vue({
    el: '#app',

    data: {
        message: 'Listado de alumnos',
        desde: null,
        listado: null
    },

    created: function () {
        this.fetchData();
    },

    methods: {
        fetchData: function () {
            var xhr = new XMLHttpRequest();
            var self = this;
            xhr.open("GET", apiURL);
            xhr.onload = function () {
                var result = JSON.parse(xhr.responseText);
                self.desde = result.desde;
                self.listado = result.alumnos;
                console.log(self.desde);
                console.log(self.listado[0]);
            };
            xhr.send();
        }
    }

});
