
var apiURL = "/alumnos/";

var app = new Vue({
    el: '#app',

    data: {
        message: 'Listado de asistencias del alumno',
        matricula: '',
        recibido: false,
        alumno: null,
        listado: null
    },

    methods: {
        fetchData: function (event) {
            console.log(event);
            var xhr = new XMLHttpRequest();
            var self = this;
            xhr.open("GET", apiURL + self.matricula);
            xhr.onload = function () {
                var result = JSON.parse(xhr.responseText);
                self.alumno = result.alumno;
                self.listado = result.asistencias;
                console.log(self.alumno);
                console.log(self.listado[0]);
                self.recibido = true;
            };
            xhr.send();
        }
    }

});
