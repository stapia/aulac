import pandas as pd

def listado():
    return pd.read_csv('alumnos-por-equipo.tsv', delimiter='\t')

def alumno(listado, mat):
    try:
        alumno = listado.loc[listado['Matricula'] == mat].iloc[0]
        datos = [ str(alumno.Matricula), alumno.EQUIPO, alumno.NOMBRE ]
        return datos
    except:
        return [str(mat), 'Undefined', 'Not found']

if __name__ == "__main__":
    listado_alumnos = listado()
    print( alumno(listado_alumnos, 19016) )
    print( alumno(listado_alumnos, 99999) )