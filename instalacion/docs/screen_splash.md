
# Instalaci�n/ejecuci�n de los script de arranque.

## Ejecutar scripts en el arranque.

La mejor opci�n es usar el servicio local que ejecuta todo 
lo que hay en la carpeta /etc/local.d siempre que tenga la extensi�n
start (para boot) o stop (para shutdown). Hay que:

```sh
rc-update add local default
rc-service local start
```

Los script se ponen en /etc/local.d y tienen que ser ejecutables. 

El script que hay que arrancar es /root/aulac/instalacion/splash-screen/splash.sh,
para ello lo m�s sencillo es ponerle un soft link en local.d que se llame splash.start

Para instalar el python3 (y que funcione) hay que acualizar (upgrade) el sistema.

```
apk update
apk upgrade
```

Y luego ya instalar el python3 y la librer�a gr�fica:

```
apk add python3 python3-tkinter
```

Hay que crear la carpeta 
```
/root/log
```

SNAPSHOTS

Los snapshots internos que ten�amos el a�o pasado no va demasiado bien y tardar 
mucho, he estado probando y son mucho mejores lo que vienen indicados aqu�:

[https://dustymabe.com/2015/01/11/qemu-img-backing-files-a-poor-mans-snapshotrollback/]
[https://fabianlee.org/2018/09/24/kvm-implementing-linked-clones-with-a-backing-file/]
[https://kashyapc.fedorapeople.org/virt/lc-2012/snapshots-handout.html]

De hecho en este �ltimo viene la manera de enlace backfile respecto de otro backfile.
No creo que nos vaya a hacer falta, pero no est� de m�s saberlo. 

El comando b�sico para hacer los snapshots es:

```
qemu-img create -f qcow2 -b backfile.img new_image.qcow2.snap
```

Para revertir el cambio simplemente hay que borrar el snap y volverlo a crear y
esa operaci�n es muy r�pida. Se puede hacer al principio del arranque. 
