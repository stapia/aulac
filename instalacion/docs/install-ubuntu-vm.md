
# Instalar máquina virtual de Ubuntu 18.04

Simplemente es para anotar que se puede crear la primera "base" de 
Ubuntu 18.04 con una mini-iso que se puede descargar de:

[https://help.ubuntu.com/community/Installation/MinimalCD]

Y se puede poner con un disco de 4 Gb. 

Al instalar yo dejaría que el usuario "alumno" fuese administrador 
y metería la típica contraseña que le ponemos siempre. 

Esta imagen no lleva prácticamente nada. Será necesario instalar, al menos:

```
sudo apt install geany gcc
```

