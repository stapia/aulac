# Sparse file

## Pasos buenos

Es necesario instalar tar y pigz:

```
apk add tar
apk add pigz
```


Para comprimir los archivos y subirlos hay que pasarlos por tar y por pigz:

```
tar Scf - disco.qcow2 | pigz -9 > disco.qcow2.tar.gz
```

Donde las opciones de tar son: "c" para crear "f -" para poner el archivo en
el stdout, "S" para Sparse y "disco.qcow2" para el nombre del sparse. 

A su vez en el pigz el -9 es la compresión 9 es la misma que --best, pero 
casi mejor poner --fast para que vaya más rápido porque tarda lo suyo. 

Lo que todavía no sé es si es mejor partir del archivo sparse o del no
sparse... Posiblemente de lo mismo, pero no lo sé.

Para ver el tamaño de los archivos sparse se puede usar: 

```
ls -lsha
```

Para descomprimir hay que usar tar y pigz, así:

```
tar -I pigz -xf disco.qcow2.tar.gz 
```
Una vez descomprimidos movemos los discos a una carpeta qu ecrearemos en root/ que pdoemos llamar VirtDisk.

Creamos la máquina virtual con Virtual Machine Manager, importando la imagen existente.

## Pasos descartados

¡ Ojo con todo esto la vez que lo he hecho se quedo el sistema inútil !

Aparentemente lo único que si hay que hacer es instalar el tar de GNU 
para que coja la opción -S para archivos sparse. 

Las imagenes mejor sparse, pero el problema es que ni el cp ni el tar
de busybox tiene la opción para manejar estos archivos, así que hay 
que sustituirlo.

Empezamos por lo que pone aquí:

<https://wiki.alpinelinux.org/wiki/How_to_get_regular_stuff_working>

Después de poner bash y las herramientas de GNU linux ya se puede 
utilizar archivos sparse. 

Para utilizar los archivos sparse se explican en:

<https://wiki.archlinux.org/index.php/sparse_file>
