# Actualizar una máquina virtual

## Aclaración previa (aviso!!)

Las máquinas virtuales tienen puesto como disco duro 
un snapshot, de manera que:

* Se borra el snapshot 
* Se genera de nuevo a partir del backing disk
* Se arranca habiendo descartados los últimos cambios

Accediendo desde la consola de administración el primer paso
no se realiza y, por tanto, la máquina virtual aparentemente
no está congelada, sin embargo, esto es engañoso porque 
en cuanto a que las modificaciones no se llevan al backing disk
y, en consecuencia, en realidad no se actualiza nada.

A 26/02/2020 he probado a hacer: 

```
# NO funciona!
qemu-img commit disco.qcow2.snap
```

Y aparentemente funciona, pero luego falla al arrancar la
máquina sin que sepamos que es lo que va mal. Así pues la 
solución pasa por el procedimiento a continuación.


## Procedimiento para actualizar

* Ejecutar el gestor de máquinas virtuales
* Abrir propiedades de la máquina y encontrar el virt disk
* Cambiar el archivo de disco (normalmente se llama disco.qcow2)
* Arrancar la máquina y actualizarla
* Apagar la máquina e inmediantamente ¡CAMBIAR otra vez el 
DISCO! El nombre está vez es algo así como disco.qcow2.snap
