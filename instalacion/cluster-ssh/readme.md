# Administracion con ssh 

Con cluster ssh se pueden hacer varias sesiones a la vez de
ssh con los equipos (el sistema base) de todos los ordenadores.

Solo hace falta instalar cluster ssh en el ordenador que administra:

```
sudo apt install clusterssh
```

Luego hay que hacer: 

```
cssh -l administrador --cluster-file cluster.txt aula3-A
cssh -l administrador --cluster-file cluster.txt aula3-B
cssh -l administrador --cluster-file cluster.txt aula3-C
cssh -l administrador --cluster-file cluster.txt aula3
```

El argumento después del archivo es el cluster, he dividido 
las IPs de 10 en 10 para que no haya demasiadas pantallas a 
la vez, pero si algo largo de hacer es mejor coger el último
argumento donde si están todas.

De 6 en 6: 

```
cssh -l administrador --cluster-file cluster-06.txt aula3-A
cssh -l administrador --cluster-file cluster-06.txt aula3-B
cssh -l administrador --cluster-file cluster-06.txt aula3-C
cssh -l administrador --cluster-file cluster-06.txt aula3-D
cssh -l administrador --cluster-file cluster-06.txt aula3-E
```
