#!/bin/sh

echo "Executing start.sh script" | logger -t "SplashScreen"

trap 'terminate' TERM 

terminate()
{
    echo "Caught SIGTERM in splash script" | logger -t "SplashScreen"
    domains=`/usr/bin/virsh list --state-running --name`
    for dom in $domains; do
        /usr/bin/virsh shutdown $dom 2>&1 | logger -t "SplashScreen Cerrando $dom"
    done
    pyID=`ps -o pid,comm -a | grep python3 | awk '{ print $1 }'`
    echo "Intentando salir de python corriendo en $pyID" | logger -t "Cerrando python3"
    kill pyID 2>&1 | logger -t "kill python"
    selection="quit"
    echo "End of terminate at splash script" | logger -t "SplashScreen"
}

cd /root/aulac/instalacion/splash-screen/

selection=`xinit /usr/bin/python3 menu.py 2> /root/log/select.log --`
echo "The selection was $selection" | logger -t "SplashScreen"

while [ $selection != "quit" ] && [ $selection != "poweroff" ]
do
    echo $selection | logger -t "SplashScreen"
    $selection
    selection=`xinit /usr/bin/python3 menu.py 2>> /root/log/select.log --`
    echo "The selection was $selection" | logger -t "SplashScreen"
done

echo "Exiting loop" | logger -t "SplashScreen"

if [ $selection = "poweroff" ]
then
    poweroff
fi
