#!/bin/sh

# Esto es lo que congela
rm /var/lib/libvirt/images/ubuntu.qcow2.snap
qemu-img create -f qcow2 -b /var/lib/libvirt/images/ubuntu.qcow2 /var/lib/libvirt/images/ubuntu.qcow2.snap

/usr/bin/virsh start ubuntu 2>&1 | logger -t "Arrancando ubuntu" 
/usr/bin/xinit /usr/bin/remote-viewer --kiosk --kiosk-quit on-disconnect -f spice://localhost:5900 2>&1 | logger -t "remote-viewer para ubuntu" --

running=`/usr/bin/virsh list --state-running --uuid`

while [ $running ]
do
    /usr/bin/virsh shutdown ubuntu 2>&1 | logger -t "Cerrando ubuntu" 
    sleep 2s
    running=`/usr/bin/virsh list --state-running --uuid`
done
