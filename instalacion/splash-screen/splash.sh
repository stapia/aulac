#!/bin/sh

# This file should be softlink into /etc/local.d/splash.start
# in order to get executed at boot
# ln -s /root/aulac/instalacion/splash-screen/splash.sh /etc/local.d/splash.start


echo "Executing splash.sh script" | logger -t "SplashScreen"

cd /root/aulac/instalacion/splash-screen/

nohup ./start.sh 2>&1 | logger -t "SplashScreen Cerrando $dom" &
