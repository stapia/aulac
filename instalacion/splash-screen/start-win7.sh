#!/bin/sh

# Esto es lo que congela
rm /var/lib/libvirt/images/win7.qcow2.snap
qemu-img create -f qcow2 -b /var/lib/libvirt/images/win7.qcow2 /var/lib/libvirt/images/win7.qcow2.snap

/usr/bin/virsh start win7 2>&1 | logger -t "Arrancando win7"
/usr/bin/xinit /usr/bin/remote-viewer --kiosk --kiosk-quit on-disconnect -f spice://localhost:5900 2>&1 | logger -t "remote-viewer win7"  --

running=`/usr/bin/virsh list --state-running --uuid`

while [ $running ]
do
    /usr/bin/virsh shutdown win7 2>&1 | logger -t "Cerrando win7"
    sleep 2s
    running=`/usr/bin/virsh list --state-running --uuid`
done
