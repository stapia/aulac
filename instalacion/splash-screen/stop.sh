#!/bin/sh

# This file should be softlink into /etc/local.d/splash.stop
# in order to get executed at poweroff time

echo "Executing stop script" | logger -t "SplashScreen"
domains=`/usr/bin/virsh list --state-running --name`
for dom in $domains; do
	/usr/bin/virsh shutdown $dom 2>&1 | logger -t "SplashScreen Cerrando $dom"
done

pyID=`ps -o pid,comm -a | grep python3 | awk '{ print $1 }'`
echo "Intentando salir de python corriendo en $pyID" | logger -t "SplashScreen"
kill pyID 2>&1 | logger -t "SplashScreen"

echo "End of stop" | logger -t "SplashScreen"
