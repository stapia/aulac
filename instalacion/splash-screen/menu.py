#!/usr/bin/python3

from datetime import datetime
import requests
import configparser
import tkinter as tk


class App:

    def __init__(self):
        # print("Init")
        pass

    def set_up(self, master):
        config = configparser.ConfigParser()
        config.read('menu-items.ini')

        # Keep master for later
        self.master = master

        self.canvas = tk.Canvas(master, width=1440, height=900, cursor="arrow")
        self.canvas.pack()

        self.background_image = tk.PhotoImage(file="img-background.png")
        self.canvas.create_image(
            0, 0, image=self.background_image, anchor=tk.NW)

        self.button_image = {}

        for item in config.sections():
            if not "icon" in config[item]:
                print("Error: missing icon in : " + item)
                return False

            self.button_image[item] = tk.PhotoImage(file=config[item]["icon"])
            #b_width = self.button_image["icon"].width()
            #b_height = self.button_image["icon"].height()
            button = tk.Button(self.canvas, image=self.button_image[item],
                               command=self.the_command(config[item]["command"]))
            self.canvas.create_window(
                config[item]["x"], config[item]["y"], window=button, anchor=tk.NW)

        self.matricula = tk.Entry(self.canvas, font="Courier 20")
        self.canvas.create_window(600, 500, window=self.matricula)

        return True

    def enviar_asistencia(self, actual_cmd):
        payload = { 'cmd': actual_cmd.split("/").pop() }
        try:
            # url = f'http://138.100.77.111:5000/asistencia/{self.matricula.get()}'
            # url = f'http://138.100.77.111:8100/api/asistencia/{self.matricula.get()}'
            url =   f'http://138.100.77.232:28080/api/asistencia/{self.matricula.get()}'
            response = requests.request('PUT', url, data=payload)
            return 'OK' in response.text  # Contiene OK y comilla y un salto de linea
        except:
            return True

    # Wrapper to encapsulate something like a lambda function...
    # To be used in the button command

    def the_command(self, cmd):
        def item_command(app=self, actual_cmd=cmd):
            app.enviar_asistencia(actual_cmd)
            print(actual_cmd)
            app.master.destroy()
        return item_command


root = tk.Tk()

# Esto es el que quita los bordes y todo eso.
root.wm_attributes('-type', 'splash')

# Esto fija el foco para que salga el editable activo
root.focus_force()

app = App()

if app.set_up(root):
    #print("Enter mainloop")
    root.mainloop()

# print("Exiting...")
