var asignaturas=0;
var lista_asignaturas=0;
$(document).ready(function(){
	$("#modelo_alumno_asignatura").hide();
	$.post('/api/getAsignaturas',
			function(r){
		var res=JSON.parse(r);
		asignaturas=res.length
		lista_asignaturas=res;
		res.forEach(function(e){
			$(".na-asignaturas").append("<option value='"+e.id+"'>"+e.codigo+' - '+e.nombre+"</option>");
		});
	});
	$("#cargar_alumnos").dropzone({ url: "/file/post",
		paramName: "fichero_alumnos_nuevos",
		maxFilesize: 15,
		maxFiles:1,
		autoProcessQueue: false,
		addRemoveLinks: true,
		acceptedFiles: "text/xml",
		accept: function(file, done) {
		    alert("hola");
		  },
		success:function(){},
		error:function(){},
	});
	
	$("#na_add_asignatura").on('click',function(){
		asignaturas--;
		if(asignaturas<=0){
			$("#na_add_asignatura").prop('disabled','disabled');
		}
		$(".chosen-select").chosen('destroy');
		var copia=$("#modelo_alumno_asignatura").clone().attr('style','margin-bottom:1%');
		copia.find('.na-asignaturas').addClass('chosen-select');
		copia.find('.na_grupo').addClass('chosen-select');
		copia.find('.na-asignaturas').prop('name','na_asignaturas');
		copia.find('.na_grupo').prop('name','na_grupo');
		$("#na_asignatura_grupos").append(copia.show());
		
		$(".chosen-select").chosen({no_results_text: "No hay resultados"});
	});
	$("#na-reset").click(function(){
		asignaturas=lista_asignaturas.length;
		$("#na_asignatura_grupos").empty();
		$("#na_add_asignatura").prop('disabled',asignaturas<=0);
	});
	$("#form_nuevo_alumno").submit(function(e) {
		
	    e.preventDefault();
	    var data={};
	    var aux;
	    data["na_asignaturas"]=[];
	    data["na_grupo"]=[];
	    	$(".na-input").each(function(){
	    		if(this.name==="na_asignaturas" || this.name==="na_grupo"){
	    			data[this.name].push(this.value);
	    		}else{
	    		data[this.name]=this.value;
	    		}
	    	});
	    	 $.post('/api/addNuevoAlumno',data,function(data,status){
	    		 if(status==='success'){
	    			 if(data.search("Error")!==-1){
	    				 modal_warning(data);
	    			 }else{
	    				 modal_correcto("Alumno guardado en la base de datos y matriculado");
	    			 }
    			 }else{
    				 modal_warning("Problema al subir el archivo. No se realizaron acciones");
    			 }
	    	 }
	  			   );
	});
});

