
$(document).ready(function() {
	CodeMirror.fromTextArea(document.getElementById("enunciado"), {
		lineNumbers: true, //Muestra los numeros de linea
		matchBrackets: true, //Muestra con que parentesis, llave o corchete corresponde el que tenemos al lado del cursorsor
		mode: "text/x-csrc", //Modo Lenguaje C
		indentUnit:1, //La longitud de la autotabulacion
		smartIndent:false 
	});
	CodeMirror.fromTextArea(document.getElementById("np_enu_codigo"), {
		lineNumbers: true, //Muestra los numeros de linea
		matchBrackets: true, //Muestra con que parentesis, llave o corchete corresponde el que tenemos al lado del cursorsor
		mode: "text/x-csrc", //Modo Lenguaje C
		indentUnit:1, //La longitud de la autotabulacion
		smartIndent:false
	});
	CodeMirror.fromTextArea(document.getElementById("np_corrector"), {
		lineNumbers: true, //Muestra los numeros de linea
		matchBrackets: true, //Muestra con que parentesis, llave o corchete corresponde el que tenemos al lado del cursorsor
		mode: "text/x-csrc", //Modo Lenguaje C
		indentUnit:1, //La longitud de la autotabulacion
		smartIndent:false 
	});
		});