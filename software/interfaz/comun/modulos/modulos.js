function cargar_modulo(m){
    var m_name=m.attr('data-modulo');
    var m_folder=m.attr('data-folder');
    $("#contenido_pagina").load(m_folder+m_name+'.html',function(){
        $.getScript(m_folder+m_name+'.js');
    });
}

function modal_correcto(str){
	document.getElementById("modal_mensaje_correcto").innerHTML = str;
	$("#modalCorrecto").modal('show');
} 
function modal_warning(str){
	document.getElementById("modal_mensaje_warning").innerHTML = str;
	$("#modalWarning").modal('show');
} 

$(document).ready(function(){
		$(".modulo").on('click',function(){
	    	$("#contenido_pagina").empty();
	    	cargar_modulo($(this));
	    });
});
