<?php 

$host_name = "localhost";
$database   = "examen_programacion";
$user_name  = "postgres";
$db = new PDO("pgsql:host=$host_name;
				port=5432;
				dbname=$database;", $user_name, 'its@2017');


function dbQuery($query, $parameters){
    global $db;
    $stmt = $db->prepare($query);
    $result = $stmt->execute($parameters);
    if(!$result){
        $errorCode = $stmt->errorCode();
        $errorInfo = $stmt->errorInfo();
        if(substr($errorCode,0,2) == "IT"){
            error(substr($errorCode,2,3), explode("CONTEXT",$errorInfo[2])[0]);
        }
        
        header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, 500);
        $explicacion = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $errorInfo[2]);
        
        $e = new Exception;
        $stackTrace = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $e->getTraceAsString());
        echo "Error SQL(" . $errorCode . "): <hr>" . $explicacion . "<hr>" . $stackTrace;
        die();
    }
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}
function dbUpdate($query, $parameters){
    global $db;
    $stmt = $db->prepare($query);
    $result = $stmt->execute($parameters);
    if(!$result){
        $errorCode = $stmt->errorCode();
        $errorInfo = $stmt->errorInfo();
        if(substr($errorCode,0,2) == "IT"){
            error(substr($errorCode,2,3), explode("CONTEXT",$errorInfo[2])[0]);
        }
        
        header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, 500);
        $explicacion = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $errorInfo[2]);
        
        $e = new Exception;
        $stackTrace = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $e->getTraceAsString());
        echo "Error SQL(" . $errorCode . "): <hr>" . $explicacion . "<hr>" . $stackTrace;
        die();
    }
    return $stmt->rowCount();
}
function dbInsert($query, $parameters){
    global $db;
    $stmt = $db->prepare($query);
    $result = $stmt->execute($parameters);
    if(!$result){
        $errorCode = $stmt->errorCode();
        $errorInfo = $stmt->errorInfo();
        if(substr($errorCode,0,2) == "IT"){
            error(substr($errorCode,2,3), explode("CONTEXT",$errorInfo[2])[0]);
        }
        
        header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, 500);
        $explicacion = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $errorInfo[2]);
        
        $e = new Exception;
        $stackTrace = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $e->getTraceAsString());
        echo "Error SQL(" . $errorCode . "): <hr>" . $explicacion . "<hr>" . $stackTrace;
        die();
    }
    return $db->lastInsertId();
}
function beginTransaction(){
    global $db;
    $db->beginTransaction();
}
function rollback(){
    global $db;
    $db->rollBack();
}
function commit(){
    global $db;
    $db->commit();
}
?>
