<?php 
$comando = $_REQUEST["comando"]; 

if($comando == 'addNuevoAlumno'){
    if(isset($_POST["na_nombre"]) && isset($_POST["na_apellidos"]) && isset($_POST["na_nmat"]) && isset($_POST["na_email"]) && isset($_POST["na_titulacion"])){
    	if(esProfesor()){
    	    $resultado = dbQuery('SELECT usuarios.nuevo_alumno(?,?,?,?,?) as id;',array($_POST["na_nombre"],$_POST["na_email"],$_POST["na_nmat"],$_POST["na_apellidos"],$_POST['na_titulacion']));
            if(!is_null($resultado)){
                if(isset($_POST["na_asignaturas"]) && isset($_POST["na_grupo"])){
                    $id_alumno=$resultado[0]['id'];
                        $asignaturas=$_POST["na_asignaturas"];
                        $grupo=$_POST["na_grupo"];
                    
                    if ( count($asignaturas) == count($grupo)){
                        for($i=0;$i<count($asignaturas);$i++){
                            $resultado = dbQuery('INSERT INTO usuarios.alumnos_asignaturas(alumno,asignatura,grupo,curso) values(?,?,?,(SELECT id FROM usuarios.cursos where now() BETWEEN f_ini AND f_fin)) returning id',array($id_alumno,$asignaturas[$i],$grupo[$i]));
                            if(!is_null($resultado)){
                                echo json_encode($resultado);
                            }else{
                                echo 'Error: Se creó usuario pero no se matriculó';
                            }  
                        }
                    }else{
                        echo 'Error: Se creó usuario pero no se matriculó';
                    }
                }else{
                    echo json_encode($resultado);
                }
            }else{
                echo 'Error: Fallo al crear el usuario';
            }            
    	}else{
    		echo 'Error: No autorizado';
    	}
    }else{
        echo 'Error: Faltan Datos';
    }
}
?>