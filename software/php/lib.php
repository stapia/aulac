<?php
function error($num, $mensaje){
	$msgCorto = "Unknown";
	if($num==404){
		$msgCorto = "Not found";
	}else if($num==401){
		$msgCorto = "Unauthorized";
	}else if($num==400){
		$msgCorto = "Bad request";
	}
	header($_SERVER["SERVER_PROTOCOL"]." $num $msgCorto", true, $num);
	echo $mensaje;
	die();
}
function validateDate($date){
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}

?>