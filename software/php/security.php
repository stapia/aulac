<?php
require_once("lib.php");
require_once("db.php");

session_start();

date_default_timezone_set('Europe/Madrid');
if(userLogged() && expiredSession()){
	logout();
}
header("Expires: Fri, 01 Jan 1999 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
$_SESSION['SKey'] = uniqid(mt_rand(), true);
$_SESSION['LastActivity'] = $_SERVER['REQUEST_TIME'];

function logOut() {
	session_unset();
	session_destroy();
	session_start();
	session_regenerate_id(true);
}

function esAdmin(){
	return isset($_SESSION["id_admin"]);	
}

function esProfesor(){
	return isset($_SESSION["id_profesor"]);
}

function esAlumno(){
	return isset($_SESSION["id_alumno"]);
}

function userLogged(){
	return isset($_SESSION['registered']) && isset($_SESSION['HTTP_USER_AGENT']) && $_SESSION['registered']==1 && $_SESSION['HTTP_USER_AGENT'] == $_SERVER['HTTP_USER_AGENT'];
}

function getUserId(){
	if(isset($_SESSION['sim_userid'])){
		return $_SESSION['sim_userid'];
	}
	return $_SESSION['userid'];
}

function userId(){
	if(isset($_SESSION['sim_userid'])){
		return $_SESSION['sim_userid'];
	}
	return $_SESSION['userid'];
}

function login($username, $password){
	
	
	if(isset($_SESSION["token"])) {
		$res = dbQuery("SELECT usuarios.session_close(?);", array($_SESSION["token"]));
	}
	
	$login = dbQuery("SELECT usuarios.login(?,?);", array($username, $password));
	$datos = json_decode($login[0]["login"],true);
	
	if(isset($datos["id_sesion"])){
		iniciarSesion($datos["id_sesion"]);
		return true;
	}else{
		return false;
	}
}
	
function iniciarSesion($idSesion){
	
	$sesion = dbQuery("SELECT usuarios.session_start(?);",array($idSesion));
	$result = json_decode($sesion[0]["session_start"],true);
	
	logOut();
	
	if(!isset($result["id"])){
		return;
	}
	
	$timeNow = time();
	$expireSessionTime = $timeNow + 3*3600; //tres horas;
	$_SESSION['expire_time']=$expireSessionTime;
	$_SESSION['registered']=1;
	$_SESSION["username"]=$result["correo_upm"];
	$_SESSION["userid"]=$result["id"];
	$_SESSION["token"]=$result["token"];
	$_SESSION["nombre_completo"]=$result["nombre"]." ".$result["apellidos"];
	
	$profesor = $result["profesor"];
	$admin = $result["administrador"];
	$alumno= $result["alumno"];
	if(isset($profesor) && $doctor != "null"){
		$_SESSION["id_profesor"] = $profesor;
	}
	if(isset($admin) && $admin != "null"){
		$_SESSION["admin"] = $admin;
	}
	if(isset($alumno) && $alumno != "null"){
		$_SESSION["id_alumno"] = $alumno;
	}
	$_SESSION["HTTP_USER_AGENT"]=$_SERVER["HTTP_USER_AGENT"];
	
}
function getUsername(){
	return $_SESSION["username"];
}
$_SERVER["DOCUMENT_HANDLE"] = $_SERVER["DOCUMENT_ROOT"];
function expiredSession(){
	return $_SESSION['expire_time'] < time();
}

function redirect_index(){
    if(isset($_SESSION['token']) && !expiredSession()){
        if(esProfesor()){
            header('Location: /profesor');
            die();
        }else if(esAlumno()){
            header('Location: /alumno');
            die();
        }
    }else{
        logout();
        header('Location: /login');
        die();
    }
}

?>